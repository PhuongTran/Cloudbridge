
new WOW().init();

/* ===== Swiper ===== */
function swiper(clswiper, clpagination){
	$(clswiper).swiper({
		pagination: clpagination,
		paginationClickable: true
		
	});
	
}

/* ===== CounterUp ===== */
function counterUpnumber(clcun, clcuntime){
	$(clcun).counterUp({
		time: clcuntime
	});
}

/* ===== Var ===== */
var clswiper ='.swiper-quote',
	clpagination = '.pagination-quote',
	numbervalue = '.number-value',
	pricevalue = '.price-value',
	clcuntime = 1000;

function heightwindow (){
	var hWindow = $(window).outerHeight(),
		hHeader = $('header').outerHeight(),
		hFooter = $('#footer-container').outerHeight(),
		hLogin = $('.container-full');
	hLogin.css('min-height', hWindow - ( hHeader + hFooter ) + 'px');
}

function inputfocus(formgroup,inputtext){
	$(inputtext).each(function() {

		$(this).on('focus', function() {
		  $(this).parent(formgroup).addClass('active');
		});
		$(this).on('blur', function() {
		  if ($(this).val().length == 0) {
			$(this).parent(formgroup).removeClass('active');
		  }
		});
		if ($(this).val() != '') $(this).parent(formgroup).addClass('active');
	  });
}
	
$(document).ready(function(){
	
	heightwindow();
	
	$(window).resize(function(){
		heightwindow ();
	});
	
	/* ----- Animation  Form Group ----- */
	var formgroup = $(".form-group"),
		inputtext = $(".form-group input");
	inputfocus(formgroup, inputtext);
	
	/* ----- Show Pass ----- */
	$(".field-password").append( "<span class='showpass'></span>" );
	$(".showpass").click(function(){
		if($('.field-password input').attr('type') == 'password'){
			$('.field-password').find('input:password').prop({type:"text"});
			$('.field-password').addClass('field-password-active');
		}else{
			$('.field-password').find('input:text').prop({type:"password"});
			$('.field-password').removeClass('field-password-active');
		}
        
    });
	

	
	
	$(window).scroll(function() {
		var headercontainer = $("header"),
			maincontainer   = $("#main-container"),
			docscroll = $(document).scrollTop(),
			HeightFixHeader = headercontainer.innerHeight();
		if (docscroll >= HeightFixHeader) {
			headercontainer.addClass("head-active");
			maincontainer.css( "margin-top", HeightFixHeader);
		}else if (docscroll < HeightFixHeader + 1) {
			headercontainer.removeClass("head-active");
			maincontainer.css( "margin-top", "" );
		}
	});

	
	swiper(clswiper, clpagination); /* ===== Quote Slider ===== */
	counterUpnumber (numbervalue, clcuntime); /* ===== Value Number ===== */
	counterUpnumber (pricevalue, clcuntime); /* ===== Price Number ===== */
	
	$('.show-list').click(function(){
		if ($('.function-list-sub').css('display') == 'none'){
			$('.function-list-sub').slideDown(200);
			$('.show-list').addClass('active');
		}else{
			$('.function-list-sub').slideUp(200);
			$('.show-list').removeClass('active');
		}
		
	});
	
	/* Video Show Modal */
	
	$('.a-video').each(function(){
				
		var url = $(this).find('img').attr('data-video'),
			videoId = url.match(/youtube\.com.*(\?v=|\/embed\/)(.{11})/).pop();
		
		$(this).find('img').attr('src', 'https://img.youtube.com/vi/' + videoId + '/maxresdefault.jpg');
	
	});

	var videoPopup = $('.video-popup');
	
	$('.video-wrapper').click(function(e) {
		var url = $(this).find('img').attr('data-video');
		videoPopup.toggleClass('open').find('iframe').attr('src', url + "&output=embed");
	});
	
	$('#close-video-popup').click(function() {
		videoPopup.toggleClass('open').find('iframe').attr('src', '');
	});
	
	$('#video-popup').click(function() {
		videoPopup.toggleClass('on-hide');
		var time = setTimeout(function() {
			videoPopup.toggleClass('on-hide');
			videoPopup.toggleClass('open').find('iframe').attr('src', '');
			clearTimeout(time);
		}, 0);
	});

});	

